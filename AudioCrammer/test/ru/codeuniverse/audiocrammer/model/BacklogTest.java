package ru.codeuniverse.audiocrammer.model;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import junit.framework.Assert;
import ru.codeuniverse.audiocrammer.model.AudioRecord;

public class BacklogTest {
	private static Dao<AudioRecord, Integer> ReadOnlyAudioRecordDAO;
	private static Path trackedAudioDir;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
		trackedAudioDir = Paths.get("Q:\\AudioCrammer\\TrackedAudio");

		// get read-only DAO for AudioRecord
		String dbURL = "file:///Q:/AudioCrammer/audiocrammer.db?mode=ro";
		// String dbURL = "Q:\\AudioCrammer\\audiocrammer.db";
		String connectionString = "jdbc:sqlite:" + dbURL;
		ConnectionSource connectionSource = new JdbcConnectionSource(connectionString);
		ReadOnlyAudioRecordDAO = DaoManager.createDao(connectionSource, AudioRecord.class);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
		ReadOnlyAudioRecordDAO.getConnectionSource().close();
	}

	@Test
	public void myTest() throws SQLException {
		System.out.println("just test");
		// ReadOnlyAudioRecordDAO.create(new AudioRecord());
		System.out.println(ReadOnlyAudioRecordDAO.countOf());
		int x = 9;
		Assert.assertEquals(9, x);
	}
}

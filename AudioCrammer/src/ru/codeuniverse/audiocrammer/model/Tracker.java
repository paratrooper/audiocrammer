package ru.codeuniverse.audiocrammer.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.dao.Dao;

/**
 * Base class for classes BacklogTracker and ReviewTracker which track audiofiles and their
 * metainfo (AudioRecord) for added and not reviewed audio (BacklogTracker) and already
 * checked-out from backlog and scheduled for first review and subsequent reviews
 * (ReviewTracker). Enables communication with DB and has some more common methods/fields.
 */
public abstract class Tracker {

	private static final Logger logger = LoggerFactory.getLogger(Tracker.class);

	// ORMLite audioRecordDAO
	protected Dao<AudioRecord, Integer> audioRecordDAO;

	// here app stores all its audiodata it works with
	protected Path trackedAudioDir;

	protected String userOutputDir;

	private Settings settings;

	protected Tracker() throws SQLException {
		settings = new Settings();
		String trackedAudioDirStr = settings.getTrackedAudioDir();
		trackedAudioDir = Paths.get(trackedAudioDirStr);
		userOutputDir = settings.getTodayStudyAudioOutputDir();

		AudioRecordDAOManager audioRecordDAOManager = new AudioRecordDAOManager();
		// returns ref to unique DAO instance for AudioRecord class
		audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
	}

	/**
	 * plusNDays("2017-03-17", 3) returns "2017-03-20"
	 * 
	 * @param ISODate
	 * @param nDays
	 * @return
	 */
	protected String plusNDays(String ISODate, int nDays) {
		LocalDate date = LocalDate.parse(ISODate);
		date = date.plusDays(nDays);
		return date.toString();
	}

	/**
	 * Deletes all files and subfolders with files within them in User Output Dir (see
	 * Settings.getOutputDir())
	 * 
	 * @throws IOException
	 */
	public void wipeOutputDir() throws IOException {

		Settings settings = new Settings();
		FileUtils.cleanDirectory(new File(settings.getTodayStudyAudioOutputDir()));
		logger.info("copyFromTrackedToWipedOutputDir(List<AudioRecord>): wiped "
				+ settings.getTodayStudyAudioOutputDir() + " directory");
	}

	protected void copyFromTrackedToWipedOutputDir(List<AudioRecord> recordsToCopy)
			throws IOException {

		if (recordsToCopy.isEmpty()) {
			return;
		}

		Settings settings = new Settings();
		FileUtils.cleanDirectory(new File(settings.getTodayStudyAudioOutputDir()));
		logger.info("copyFromTrackedToWipedOutputDir(List<AudioRecord>): wiped "
				+ settings.getTodayStudyAudioOutputDir() + " directory");

		for (AudioRecord audioRecord : recordsToCopy) {
			Path fromPath = Paths.get(settings.getTrackedAudioDir(), audioRecord.trackedName);
			Path toPath = Paths.get(settings.getTodayStudyAudioOutputDir(), audioRecord.trackedName);
			Files.copy(fromPath, toPath);
			logger.debug("copyFromTrackedToWipedOutputDir(List<AudioRecord>): copied file from "
					+ fromPath + " to " + toPath);
		}
	}

	/**
	 * doesn't wipe target dir (Output Dir) before. Overloaded
	 * copyFromTrackedToOutputDir(List<AudioRecord> recordsToCopy) for one AudioRecord.
	 * 
	 * @param recordToCopy
	 * @throws IOException
	 */
	public void copyFromTrackedToOutputDir(AudioRecord recordToCopy) throws IOException {

		Objects.requireNonNull(recordToCopy);

		Path fromPath = Paths.get(settings.getTrackedAudioDir(), recordToCopy.trackedName);
		Path toPath = Paths.get(settings.getTodayStudyAudioOutputDir(), recordToCopy.trackedName);
		Files.copy(fromPath, toPath);
		logger.debug("copyFromTrackedToOutputDir(AudioRecord recordToCopy): copied file from "
				+ fromPath + " to " + toPath);
	}

}

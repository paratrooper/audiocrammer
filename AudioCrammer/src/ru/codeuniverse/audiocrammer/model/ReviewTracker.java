package ru.codeuniverse.audiocrammer.model;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

public class ReviewTracker extends Tracker {

	private static final Logger logger = LoggerFactory.getLogger(ReviewTracker.class);

	public ReviewTracker() throws SQLException {

	}

	/**
	 * @return false if you still have any files, checked out earlier, to review. In other
	 *         words, there are files (records) in DB with nextReviewDate today or before today
	 *         (today=when you try checkoutNew).
	 * @throws SQLException
	 */
	public boolean hasScheduledNotReviewed() throws SQLException {
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);

		List<AudioRecord> ScheduledButNotReviewedRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().le("nextReviewDate", todayDateISO);
		PreparedQuery<AudioRecord> pq = qb.prepare();
		ScheduledButNotReviewedRecords = audioRecordDAO.query(pq);

		boolean hasScheduledNotReviewed;
		if (ScheduledButNotReviewedRecords.isEmpty()) {
			hasScheduledNotReviewed = false;
		} else {
			hasScheduledNotReviewed = true;
		}

		return hasScheduledNotReviewed;
	}

	/**
	 * Like hasScheduledNotReviewed(), but returns number of records due for review (both for
	 * today and all time before today - overdue, exactly like hasScheduledNotReviewed())
	 * 
	 * @return
	 * @throws SQLException
	 */
	public int countScheduledNotReviewed() throws SQLException {
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);
		List<AudioRecord> ScheduledButNotReviewedRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().le("nextReviewDate", todayDateISO);
		PreparedQuery<AudioRecord> pq = qb.prepare();
		ScheduledButNotReviewedRecords = audioRecordDAO.query(pq);

		return ScheduledButNotReviewedRecords.size();
	}

	/**
	 * 
	 * @return List of all AudioRecord objects whose nextReviewDate <= today date (i.e.
	 *         scheduled for review (repetition), but not reviewed yet. The returned List is
	 *         sorted first by nextReviewDate (descending, closer for today first), then within
	 *         same nextReviewDate it is sorted by origModifiedTime (ascending, oldest (within
	 *         same date) time first)
	 * @throws SQLException
	 */
	public List<AudioRecord> getAllScheduledNotReviewed() throws SQLException {
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);
		List<AudioRecord> scheduledButNotReviewedRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().le("nextReviewDate", todayDateISO);
		qb.orderBy("nextReviewDate", false); // descending
		qb.orderBy("origModifiedTime", true); // ascending
		PreparedQuery<AudioRecord> pq = qb.prepare();
		scheduledButNotReviewedRecords = audioRecordDAO.query(pq);

		return scheduledButNotReviewedRecords;
	}

	private List<AudioRecord> getAllScheduledForToday() throws SQLException {
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);
		List<AudioRecord> scheduledForTodayRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().eq("nextReviewDate", todayDateISO);
		qb.orderBy("origModifiedTime", true); // ascending
		PreparedQuery<AudioRecord> pq = qb.prepare();
		scheduledForTodayRecords = audioRecordDAO.query(pq);

		return scheduledForTodayRecords;
	}

	/**
	 * 
	 * @return due for review exactly today and any time before today
	 * @throws SQLException
	 */
	private List<AudioRecord> getAllScheduledWithOverdue() throws SQLException {
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);
		List<AudioRecord> scheduledForTodayRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().le("nextReviewDate", todayDateISO);
		qb.orderBy("origModifiedTime", true); // ascending
		PreparedQuery<AudioRecord> pq = qb.prepare();
		scheduledForTodayRecords = audioRecordDAO.query(pq);

		return scheduledForTodayRecords;
	}

	/**
	 * schedules next review date and writes it to DB, copies files to user OutputDir - for all
	 * files due for review exactly today (if due review date is before today - does nothing for
	 * such such already-missed-to-review (overdue) files).
	 * 
	 * @return duration (in minutes) of all files to review it copied to user OutputDir
	 * @throws Exception
	 */
	public int checkoutAllScheduledForToday() throws Exception {
		wipeOutputDir(); // CLEAR USER OUTPUT DIR
		List<AudioRecord> scheduledForToday = new ArrayList<>();
		scheduledForToday = getAllScheduledForToday();
		int durationMins = 0;
		for (AudioRecord audioRecord : scheduledForToday) {
			durationMins += audioRecord.fileSize / Settings.MINUTE_HAS_BYTES;
			audioRecordDAO.callBatchTasks(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					copyFromTrackedToOutputDir(audioRecord);
					scheduleNext(audioRecord);
					return null;
				}
			});
		}
		return durationMins;
	}

	/**
	 * schedules next review date and writes it to DB, copies files to user OutputDir - for all
	 * files due for review exactly today AND all files with review date before today
	 * (already-missed-to-review (overdue) files).
	 * 
	 * @return duration (in minutes) of all files to review it copied to user OutputDir
	 * @throws Exception
	 */
	public int checkoutAllScheduledWithOverdue() throws Exception {
		wipeOutputDir(); // CLEAR USER OUTPUT DIR
		List<AudioRecord> scheduledAll = new ArrayList<>();
		scheduledAll = getAllScheduledWithOverdue();
		int durationMins = 0;
		for (AudioRecord audioRecord : scheduledAll) {
			durationMins += audioRecord.fileSize / Settings.MINUTE_HAS_BYTES;
			audioRecordDAO.callBatchTasks(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					copyFromTrackedToOutputDir(audioRecord);
					scheduleNext(audioRecord);
					return null;
				}
			});
		}
		return durationMins;
	}

	/**
	 * Calculates and writes to DB: nextReviewNumber (increments by 1), nextReviewDate, updates
	 * ReviewHistory.
	 * 
	 * @param audioRecord
	 * @return ISO 8601 (extended) date of next review of AudioRecord that this method
	 *         calculated and written to DB.
	 * @throws Exception
	 */
	private String scheduleNext(AudioRecord audioRecord) throws Exception {
		// get time interval from today until next review (scheduled by this method)
		int nextReviewInterval;
		int nextReviewNumber = (audioRecord.nextReviewNumber) + 1;
		Importance importance = audioRecord.importance;
		if (importance == Importance.MID) {
			nextReviewInterval = Settings.BASIC_REVIEW_INTERVAL.get(nextReviewNumber);
		} else if (importance == Importance.HIGH) {
			nextReviewInterval = Settings.IMPORTANT_REVIEW_INTERVAL.get(nextReviewNumber);
		} else {
			nextReviewInterval = Settings.NOT_IMPORTANT_REVIEW_INTERVAL.get(nextReviewNumber);
		}

		// calculate nextReviewDate adding nextReviewInterval to today
		// FIXME for overdue interval is from today, not from scheduled due review date...
		LocalDate today = LocalDate.now();
		LocalDate nextScheduled = today.plusDays(nextReviewInterval);

		audioRecordDAO.callBatchTasks(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				audioRecord.nextReviewNumber = nextReviewNumber;
				audioRecord.nextReviewDate = nextScheduled.toString();
				audioRecord.addToReviewHistory(today.toString());
				audioRecordDAO.update(audioRecord);
				return null;
			}
		});

		return nextScheduled.toString();
	}

	/**
	 * if audio file is discontinued, all its metadata are removed from DB and the file is moved
	 * from trackedAudioDir to discontinuedDir
	 * 
	 * @param audioRecord
	 *            to be discontinued.
	 * @return DB id of discontinued audioRecord
	 * @throws Exception
	 */
	public int discontinue(AudioRecord audioRecord) throws Exception {

		int audioRecordID = audioRecord.getId();

		audioRecordDAO.callBatchTasks(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				moveFileFromTrackedToDiscontinuedDir(audioRecord);
				audioRecordDAO.delete(audioRecord);
				return null;
			}
		});

		return audioRecordID;
	}

	// move file corresponding to audioRecord
	// from trackedAudioDir to discontinuedDir
	// returns filename (my.wma for srcFile "c:\dir\my.wma") of moved file.
	// discontinuedDir shall have no duplicate filename
	private String moveFileFromTrackedToDiscontinuedDir(AudioRecord audioRecord)
			throws IOException {
		String fileName = audioRecord.getTrackedName();
		Settings settings = new Settings();
		String trackedAudioDir = settings.getTrackedAudioDir();
		Path trackedPathName = Paths.get(trackedAudioDir, fileName);
		String discontinuedDir = settings.getDiscontinuedDir();
		Path discontinuedPathName = Paths.get(discontinuedDir, fileName);
		try {
			Files.move(trackedPathName, discontinuedPathName,
					StandardCopyOption.REPLACE_EXISTING);

		} catch (FileAlreadyExistsException e) {
			logger.info(
					"Cannot move file from trackedAudioDir to discontinuedDir: discontinuedDir already has file with same name as trackedAudioDir.");
		}

		return fileName;
	}

}

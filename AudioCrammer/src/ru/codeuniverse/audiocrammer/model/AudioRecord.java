package ru.codeuniverse.audiocrammer.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

//represents physical audiofile metadata
@DatabaseTable(tableName = "AudioRecords")
public class AudioRecord {

	public void setTopicGroup(String topicGroup) {
		topicGroup.trim();
		this.topicGroup = topicGroup;
	}

	public void setTopic(String topic) {
		topic.trim();
		this.topic = topic;
	}

	private static final Logger logger = LoggerFactory.getLogger(AudioRecord.class);

	// id assigned when AudioRecord saved to db
	@DatabaseField(generatedId = true)
	int id;

	public void setImportance(Importance importance) {
		this.importance = importance;
	}

	public void setCramFlag(boolean cramFlag) {
		this.cramFlag = cramFlag;
	}

	public void setSubTopic(String subTopic) {
		subTopic.trim();
		this.subTopic = subTopic;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public void setSearchTags(String searchTags) {
		this.searchTags = searchTags;
	}

	public void setNextReviewDate(String nextReviewDate) {
		this.nextReviewDate = nextReviewDate;
	}

	// higher importance items reviewed more often
	@DatabaseField(canBeNull = false)
	Importance importance;

	// if true, this AudioRecord is reviewed in Cram Mode
	@DatabaseField
	boolean cramFlag;

	// TopicGroup - general (broadest) topic group
	// example: IT, science, law, ...
	@DatabaseField(canBeNull = false)
	String topicGroup;
	// Narrower topic (subdivision of topicGroup)
	// example (for IT): java, html, sql, ...
	@DatabaseField(canBeNull = false)
	String topic;
	// example (for java): java core, javaEE, Spring, Hibernate
	@DatabaseField
	String subTopic;

	// user text note for this AudioRecord, searchable
	@DatabaseField
	String Comment;

	// search tags - can search thru these tags only
	@DatabaseField
	String searchTags;

	// date (w/o time) when user first studied audiofile
	// ISO 8601 extended format (human readable): YYYY-MM-DD
	@DatabaseField
	String chekoutDate;

	// date when audiofile shall be studied (reviewed) next time
	// after chekoutDate or some previous review.
	// It can be before (not due) and after (study backlog) today date.
	// ISO 8601 extended format (human readable): YYYY-MM-DD
	@DatabaseField
	String nextReviewDate;

	/**
	 * after user checks-out AudioRecord for study, this day (of check-out) is review No. 0 Next
	 * review - review No. 1. Number of days between review 0 and review 1 is
	 * Settings.BASIC_REVIEW_INTERVAL.get(1)
	 */
	@DatabaseField
	int nextReviewNumber;

	/**
	 * sequence number and respective date of each ACTUAL (not just scheduled) review.
	 * chekoutDate not included in this history.
	 * <p>
	 * In ISO 8601 extended format (human readable): example: "1, 2017-04-12; 2, 2017-04-15"
	 * </p>
	 * 
	 */
	@DatabaseField
	String actualReviewHistory;

	// name under which audiofile is stored (copied) in this app internal
	// folder:
	// it can differ from orig name (when given by user) because same-name files
	// are renamed (suffix appended) when copied to this app internal folder
	@DatabaseField
	String trackedName;

	// filename of audiofile before this app processed it and (possibly) renamed
	@DatabaseField(canBeNull = false)
	String origName;

	// 2017-04-15T23:15 - when audiofile was originally recorded
	// ISO 8601 extended format (human readable)
	@DatabaseField(canBeNull = false)
	String origModifiedTime;

	// audiofile size in bytes
	@DatabaseField(canBeNull = false)
	long fileSize;

	public int getId() {
		return id;
	}

	public Importance getImportance() {
		return importance;
	}

	public boolean isCramFlag() {
		return cramFlag;
	}

	public String getTopicGroup() {
		return topicGroup;
	}

	public String getTopic() {
		return topic;
	}

	public String getSubTopic() {
		return subTopic;
	}

	public String getComment() {
		return Comment;
	}

	public String getSearchTags() {
		return searchTags;
	}

	public String getChekoutDate() {
		return chekoutDate;
	}

	public String getNextReviewDate() {
		return nextReviewDate;
	}

	public int getNextReviewNumber() {
		return nextReviewNumber;
	}

	public String getActualReviewHistory() {
		return actualReviewHistory;
	}

	public String getTrackedName() {
		return trackedName;
	}

	public String getOrigName() {
		return origName;
	}

	public String getOrigModifiedTime() {
		return origModifiedTime;
	}

	public long getFileSize() {
		return fileSize;
	}

	public AudioRecord() {
		importance = Importance.MID;
		cramFlag = false;
		topicGroup = "IT";
		topic = "Java";
		subTopic = "";
		Comment = "";
		searchTags = "";
	}

	/**
	 * Appends comma separated review dates to reviewHistory field:
	 * (revision1Date,revision2Date, etc). Checkout date not appended
	 * 
	 * @param ISO
	 *            8601 date in extended format Example: 2017-03-24
	 */
	public void addToReviewHistory(String isoDate) {
		// check we got valid extended ISO 8601 date only
		if (!isoDate.matches("\\d{4}-\\d{2}-\\d{2}")) {
			throw new IllegalArgumentException(
					"argument is not valid extended ISO 8601 date only");
		}

		String oldReviewHistory = actualReviewHistory; // for logging
		// DO NOT CHANGE ON APPS BEING ALREADY IN USE!
		// BESIDES COMPATIBILITY BREAK IS POSSIBLE WITH OLDER APPS DATABASES
		final String separator = ", ";

		if (actualReviewHistory == null) {
			actualReviewHistory = isoDate;
		} else {
			actualReviewHistory = actualReviewHistory + separator + isoDate;
		}

		logger.info("addToReviewHistory method: " + "arg given: " + isoDate
				+ "reviewHistory field before: " + oldReviewHistory
				+ "reviewHistory field after: " + actualReviewHistory);

	}

	@Override
	public int hashCode() {
		//@formatter:off
		//TODO write own hashcode for speed - not to create new HashCodeBuilder 
		return new HashCodeBuilder(7,17)
				.append(origModifiedTime)
				.append(fileSize)
				.toHashCode();
		//@formatter:on
	}

	@Override
	public boolean equals(Object obj) {

		// includes null check for obj
		if (!(obj instanceof AudioRecord)) {
			return false;
		}

		boolean origModifiedTimeEquals;
		origModifiedTimeEquals = (((AudioRecord) obj).origModifiedTime)
				.equals(this.origModifiedTime);
		boolean fileSizeEquals = (((AudioRecord) obj).fileSize) == (this.fileSize);
		if (origModifiedTimeEquals && fileSizeEquals) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		//@formatter:off
		return new ToStringBuilder(this)
				.append("id", id)
				.append("importance", importance)
				.append("cramFlag", cramFlag)
				.append("topicGroup", topicGroup)
				.append("topic", topic)
				.append("subTopic", subTopic)
				.append("Comment", Comment)
				.append("searchTags", searchTags)
				.append("chekoutDate", chekoutDate)
				.append("nextReviewDate", nextReviewDate)
				.append("reviewHistory", actualReviewHistory)
				.append("trackedName", trackedName)
				.append("origName", origName)
				.append("origModifiedTime", origModifiedTime)
				.append("fileSize", fileSize)
				.toString();
		//@formatter:on
	}

	/**
	 * compares by origModifiedTime and fileSize. for ascending sort (oldest time first). If
	 * origModifiedTime is same, fileSize compared additionally, smaller filesize first.
	 */
	public static class ModifiedTimeAscendingComparator
			implements Comparator<AudioRecord>, Serializable {

		@Override
		public int compare(AudioRecord o1, AudioRecord o2) {
			String message = "AudioRecord.ModifiedTimeComparator is given null value";
			Objects.requireNonNull(o1, message);
			Objects.requireNonNull(o2, message);

			if (o1.equals(o2)) {
				return 0;
			}

			if (o1.origModifiedTime.compareTo(o2.origModifiedTime) > 0) {
				return 1;
			} else if (o1.origModifiedTime.compareTo(o2.origModifiedTime) < 0) {
				return -1;
			} else if (o1.origModifiedTime.compareTo(o2.origModifiedTime) == 0) {

				if (o1.fileSize < o2.fileSize) {
					return 1;
				} else if (o1.fileSize > o2.fileSize) {
					return -1;
				} else { // o1.fileSize == o2.fileSize
					logger.info(
							"AudioRecord.ModifiedTimeComparator got two same objects. compare returned 0 to be consistent with equals, but control shall never reach here");
					// control shall never reach here, this case is covered by arg1.equals(arg2)
					// above
					return 0;
				}

			}
			logger.warn(
					"Control shall never reach end of AudioRecord.ModifiedTimeComparator.compare() ! Logic failed! ");
			return -1;
		}
	}

	/**
	 * compares by origModifiedTime and fileSize. for descending sort (now time first). If
	 * origModifiedTime is same, fileSize compared additionally, bigger filesize first.
	 */
	public static class ModifiedTimeDescendingComparator
			implements Comparator<AudioRecord>, Serializable {

		ModifiedTimeAscendingComparator ascendingComparator = new ModifiedTimeAscendingComparator();

		@Override
		public int compare(AudioRecord o1, AudioRecord o2) {
			return ascendingComparator.compare(o2, o1);
		}

	}

	/**
	 * compares by origModifiedTime and fileSize. for ascending sort (oldest time first). If
	 * origModifiedTime is same, fileSize compared additionally, smaller filesize first.
	 */
	public static class DescendingDateAscendingTimeOrigComparator
			implements Comparator<AudioRecord>, Serializable {

		@Override
		public int compare(AudioRecord o1, AudioRecord o2) {
			String message = "AudioRecord.ModifiedTimeComparator is given null value";
			Objects.requireNonNull(o1, message);
			Objects.requireNonNull(o2, message);

			if (o1.equals(o2)) {
				return 0;
			}

			String date1, date2;
			String time1, time2;
			date1 = StringUtils.substringBefore(o1.origModifiedTime, "T");
			date2 = StringUtils.substringBefore(o2.origModifiedTime, "T");
			time1 = StringUtils.substringAfter(o1.origModifiedTime, "T");
			time2 = StringUtils.substringAfter(o2.origModifiedTime, "T");

			if (date1.compareTo(date2) > 0) {
				return -1;
			} else if (date1.compareTo(date2) < 0) {
				return 1;
			} else if (date1.compareTo(date2) == 0) {
				System.out.println("EQ DATE");
				System.out.println("time1 = " + time1);
				System.out.println("time2 = " + time2);
				if (time1.compareTo(time2) > 0) {
					return 1;
				} else if (time1.compareTo(time2) < 0) {
					return -1;
				} else {
					logger.warn(
							"Should be no audiofile with same orig date and SAME ORIG TIME (minutes"
									+ "precision), BUT GOT ONE!");
				}

			}
			logger.warn(
					"Control shall never reach end of AudioRecord.ModifiedTimeComparator.compare() ! Logic failed! ");
			return -1;
		}
	}

}

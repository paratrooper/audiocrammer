package ru.codeuniverse.audiocrammer.model;

// high importance AudioRecords are reviewed more often
public enum Importance {
	HIGH, MID, LOW;
}

package ru.codeuniverse.audiocrammer.model;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class AudioRecordDAOManager {

	private static ConnectionSource connectionSource;

	static {
		Settings settings = new Settings();
		String dbPath = settings.getAppDir();
		String dbFileName = Settings.dbFileName;
		Path dbFile = Paths.get(dbPath, dbFileName);
		String connectionString = "jdbc:sqlite:" + dbFile;
		try {
			connectionSource = new JdbcConnectionSource(connectionString);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public AudioRecordDAOManager() throws SQLException {

	}

	/**
	 * new AudioRecordDAOManager().getORMLiteDAO() always returns same Dao instance (with same
	 * connection). To close respective connection call getConnectionSource().close() on the
	 * returned Dao object.
	 * 
	 * @return DAO object to store AudioRecord instances in database using ORM Lite. Returned
	 *         DAO object conforms to Dao<AudioRecord, Integer> interface required to call ORM
	 *         Lite methods on it.
	 * @throws SQLException
	 */
	public Dao<AudioRecord, Integer> getORMLiteDAO() throws SQLException {

		// DaoManager.createDao called multiply always returns ref to same Dao
		// obj. To close: refDAO.getConnectionSource().close() on any ref
		Dao<AudioRecord, Integer> audioRecordDAO = DaoManager.createDao(connectionSource,
				AudioRecord.class);

		// this was needed once only to create table in DB at first run
		if (!audioRecordDAO.isTableExists())
			TableUtils.createTable(connectionSource, AudioRecord.class);

		return audioRecordDAO;
	}
}
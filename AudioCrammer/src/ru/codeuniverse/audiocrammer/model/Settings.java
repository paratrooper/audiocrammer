package ru.codeuniverse.audiocrammer.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * user preferences
 */
public class Settings {

	private static final String INTERNAL_DIR_NAME = "AudioCrammer";

	private static final Logger logger = LoggerFactory.getLogger(Settings.class);

	/**
	 * if directory from which files are added to backlog has files with different extentions it
	 * is OK, but only files with this extension are recognized. See List<Path>
	 * getAllFilesInDir(Path dir) method
	 */
	static final String SUPPORTED_AUDIOFILE_EXTENTION = "*.wma";

	// Basic Review Intervals:
	// 300 - ... +30 ... + 30....
	// TODO init for many years to go (say, 90 years)...
	// REVIEW BASIC_REVIEW_INTERVAL.get(0) is nice?
	// XXX
	static final List<Integer> BASIC_REVIEW_INTERVAL;
	static {
		List<Integer> tmpLst = new ArrayList<>();
		tmpLst.addAll(Arrays.asList(0, 5, 10, 16, 45, 95, 180, 270, 300, 330, 360, 390));
		BASIC_REVIEW_INTERVAL = Collections.unmodifiableList(tmpLst);
	}

	// Important Review Intervals:
	// XXX
	// TODO init for many years to go (say, 90 years)...
	//// 300 - ... +30 ... + 30....
	static final List<Integer> IMPORTANT_REVIEW_INTERVAL;
	static {
		List<Integer> tmpLst = new ArrayList<>();
		tmpLst.addAll(
				Arrays.asList(0, 3, 7, 12, 25, 42, 110, 140, 180, 250, 300, 330, 360, 390));
		IMPORTANT_REVIEW_INTERVAL = Collections.unmodifiableList(tmpLst);
	}

	// NOT Important Review Intervals:
	// TODO init for many years to go (say, 90 years)...
	// - 380 - ... +50 ...
	// XXX
	static final List<Integer> NOT_IMPORTANT_REVIEW_INTERVAL;
	static {
		List<Integer> tmpLst = new ArrayList<>();
		tmpLst.addAll(Arrays.asList(0, 7, 14, 50, 100, 200, 300, 300, 350, 380, 430));
		NOT_IMPORTANT_REVIEW_INTERVAL = Collections.unmodifiableList(tmpLst);
	}

	// DO NOT CHANGE (for already working app with accumulated data)
	// changing this negatively affects already working app with accumulated data:
	// new empty database will be created and used
	public static final String dbFileName = "audiocrammer.db";

	// SIZE IN BYTES OF 1 SECOND OF AUDIO RECORD
	// 1 second = 15,7 kB (.WMA of my Olympus WS-811, WS-812 default settings)
	public static final long SECOND_HAS_BYTES = 15700;

	// FOR .WMA ONLY, ONLY FOR YOUR STANDARD SETTINGS
	// THINK shall be configurable?
	public static final long MINUTE_HAS_BYTES = SECOND_HAS_BYTES * 60;

	/**
	 * number of records, returned by query to DB. Used by different algorithms. For example,
	 * number of AudioRecord objects retrieved from db. CHANGE IT IF USER EVER WANTS TO CHECK
	 * OUT MORE FILES (if user wants exactly 100 maximum, make it a bit more)
	 */
	public static final int NUM_RETRIEVED_RECORDS_LIMIT = 100;

	/**
	 * Default for max duration of all files to be studied by user (either "Get for Today Study"
	 * or "Check out New")
	 */
	private static final int MAX_DURATION_DEFAULT = 45;

	/**
	 * user can set MaxDuration for Checkout New on Settings page >= this limit
	 */
	public static final Integer MAX_DURATION_MIN_LIMIT = 25;

	public static final String TOPIC_GROUP_DEFAULT = "IT";

	public static final String TOPIC_DEFAULT = "Java";

	public static final String SUB_TOPIC_DEFAULT = "";

	private static final String TODAY_STUDY_AUDIO = "TODAY_STUDY_AUDIO";

	public static final String APP_ICON = "/st.png";

	public static final String HELP_URL = "https://bitbucket.org/codeuniv/audiocrammer/wiki/edit/Home";

	private static final String SETTINGS_FILE_NAME = "settings.xml";

	private static final String INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT;
	static {
		// Alternative to user.home: https://github.com/catnapgames/WinFoldersJava
		INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT = System.getProperty("user.home");
	}

	public String getTopicGroupDefault() {
		readProperties();
		return props.getProperty("TopicGroupDefault", TOPIC_GROUP_DEFAULT);
	}

	public void setTopicGroupDefault(String topicGroupDefault) {
		props.setProperty("TopicGroupDefault", topicGroupDefault);
		saveProperties();
	}

	public String getTopicDefault() {
		readProperties();
		return props.getProperty("TopicDefault", TOPIC_DEFAULT);
	}

	public void setTopicDefault(String topicDefault) {
		props.setProperty("TopicDefault", topicDefault);
		saveProperties();
	}

	public String getSubTopicDefault() {
		readProperties();
		return props.getProperty("SubTopicDefault", SUB_TOPIC_DEFAULT);
	}

	public void setSubTopicDefault(String subTopicDefault) {
		props.setProperty("SubTopicDefault", subTopicDefault);
		saveProperties();
	}

	private Properties props;

	/**
	 * directory where app stores all its data
	 */
	private volatile String appDir;

	/**
	 * appDir default value until user changed any setting
	 */
	private final String appDirDefault;

	/**
	 * Max duration of all files to be studied by user (either "Get for Today Study" or "Check
	 * out New")
	 */
	private final int maxDuration;

	public int getMaxDuration() {
		String maxDurationDefault = String.valueOf(MAX_DURATION_DEFAULT);
		readProperties();
		return Integer.parseInt(props.getProperty("maxDuration", maxDurationDefault));
	}

	public void setMaxDuration(int maxDuration) {
		String maxDurationStr = String.valueOf(maxDuration);
		props.setProperty("maxDuration", maxDurationStr);
		saveProperties();
	}

	/**
	 * All audiofiles to be presented to user are stored here. App copies audio files given by
	 * user into this dir.
	 */
	private volatile String trackedAudioDir;

	/**
	 * trackedAudioDir default value until user changed any setting
	 */
	private final String trackedAudioDirDefault;

	/**
	 * directory which app wipes and then writes to it audiofiles for repetition (both new and
	 * scheduled for repetition) from its trackedAudioDir.
	 */
	private volatile String todayStudyAudioOutputDir;

	/**
	 * outputDir (for today study audio) default value until user changed any setting
	 */
	private final String todayStudyAudioOutputDirDefault;

	/**
	 * if audio file is discontinued, its metadata are removed from DB and the file is moved
	 * from trackedAudioDir to discontinuedDir
	 */
	private volatile String discontinuedDir;

	public String getDiscontinuedDir() {
		readProperties();
		return props.getProperty("discontinuedDir", discontinuedDirDefault);
	}

	public void setDiscontinuedDir(String discontinuedDir) {
		props.setProperty("discontinuedDir", discontinuedDir);
		saveProperties();
	}

	/**
	 * discontinuedDir default value until user changed any setting *
	 */
	private final String discontinuedDirDefault;

	/**
	 * dir for audio for Cramming - default value until user changed any setting *
	 */
	private final String cramThisDirDefault;

	// method is final because called from constructor
	public final String getAppDir() {
		readProperties();
		return props.getProperty("appDir", appDirDefault);
	}

	public void setAppDir(String appDir) {
		props.setProperty("appDir", appDir);
		saveProperties();
	}

	public String getTrackedAudioDir() {
		readProperties();
		return props.getProperty("trackedAudioDir", trackedAudioDirDefault);
	}

	public void setTrackedAudioDir(String trackedAudioDir) {
		props.setProperty("trackedAudioDir", trackedAudioDir);
		saveProperties();
	}

	public String getTodayStudyAudioOutputDir() {
		readProperties();
		return props.getProperty("todayStudyAudioOutputDir", todayStudyAudioOutputDirDefault);
	}

	public void setTodayStudyAudioOutputDir(String todayStudyAudioOutputDir) {
		props.setProperty("todayStudyAudioOutputDir", todayStudyAudioOutputDir);
		saveProperties();
	}

	public String getCramThisOutputDir() {
		readProperties();
		return props.getProperty("cramThisOutputDir", cramThisDirDefault);
	}

	public void setCramThisOutputDir(String cramThisOutputDir) {
		props.setProperty("cramThisOutputDir", cramThisOutputDir);
		saveProperties();
	}

	File autoFillPersistedFile;

	public File getAutoFillPersistedFile() {
		return autoFillPersistedFile;
	}

	// parent folder within which this App creates its own directory
	private String internalFolderParentDirectory;

	public String getInternalFolderParentDirectory() {
		readProperties();
		return props.getProperty("internalFolderParentDirectory",
				INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT);
	}

	public void setInternalFolderParentDirectory(String internalFolderParentDirectory) {
		props.setProperty("internalFolderParentDirectory", internalFolderParentDirectory);
		saveProperties();
	}

	// TODO ask appDirDefault at first start only
	// beware registry settings may exist from last start after user
	// discontinued usage long ago: ask "old settings found - delete them
	// or use them?"
	public Settings() {

		props = new Properties();

		readProperties();

		String internalFolderParentDirectory = initInternalFolderParentDirectory();

		this.appDirDefault = Paths.get(internalFolderParentDirectory, INTERNAL_DIR_NAME)
				.toString();
		this.trackedAudioDirDefault = Paths.get(appDirDefault, "TrackedAudio").toString();

		this.todayStudyAudioOutputDirDefault = Paths
				.get(INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT, TODAY_STUDY_AUDIO).toString();

		this.cramThisDirDefault = Paths.get(appDirDefault, "Cramming").toString();
		this.discontinuedDirDefault = Paths.get(appDirDefault, "Discontinued").toString();

		maxDuration = MAX_DURATION_DEFAULT;

		String appDir = this.getAppDir();
		String autoFillPersistedFileName = "autoFillSettings.ser";
		autoFillPersistedFile = new File(appDir, autoFillPersistedFileName);

	}

	private String initInternalFolderParentDirectory() {
		logger.debug("Settings.initInternalFolderParentDirectory() entered");
		readProperties();
		String internalDir = props.getProperty("internalFolderParentDirectory", null);

		if (internalDir == null) {
			try {
				internalDir = GetExecutionPath(this).toString();
				props.setProperty("internalFolderParentDirectory", internalDir);
				saveProperties();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				logger.info(
						"Settings.initInternalFolderParentDirectory() threw URISyntaxException");
				e.printStackTrace();
			}

		}

		return internalDir;
	}

	private void readProperties() {

		try {
			File settingsFileWithPath = getSettingsFileWithPath();
			FileInputStream fis = null;
			if (settingsFileWithPath.isFile()) {
				fis = new FileInputStream(settingsFileWithPath.toString());
				BufferedInputStream bis = new BufferedInputStream(fis);
				props.loadFromXML(bis);
				bis.close();
			}

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw URISyntaxException loading properties");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw FileNotFoundException loading properties");
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw InvalidPropertiesFormatException loading properties");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("class Settings: constructor threw IOException loading properties");
			e.printStackTrace();
		}

	}

	private void saveProperties() {

		try {
			File settingsFileWithPath = getSettingsFileWithPath();
			FileOutputStream fos = null;
			fos = new FileOutputStream(settingsFileWithPath.toString());
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			props.storeToXML(bos, "AudioCrammer settings");
			bos.close();

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw URISyntaxException saving properties");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw FileNotFoundException saving properties");
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw InvalidPropertiesFormatException saving properties");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("class Settings: constructor threw IOException saving properties");
			e.printStackTrace();
		}

	}

	private File getSettingsFileWithPath() throws URISyntaxException {
		Path rootPath = GetExecutionPath(this);
		Path internalDirPath = rootPath.resolve(INTERNAL_DIR_NAME);
		File settingsFileWithPath = internalDirPath.resolve(SETTINGS_FILE_NAME).toFile();
		return settingsFileWithPath;
	}

	public void resetAllSettings() throws BackingStoreException {
		props.clear();
		saveProperties();
	}

	public static class FirstAppStart {

		/**
		 * @return true only if this is the very first time this app was launched on this
		 *         computer (under this user). Different launches under different users return
		 *         true even on the same computer
		 */
		public static boolean isFirstAppStart() {
			Preferences prefs = Preferences.userNodeForPackage(Settings.class);
			String firstAppStartUp = prefs.get("firstAppStartUp", null);
			boolean isFirstAppStartUp = (null == firstAppStartUp) ? true : false;
			if (isFirstAppStartUp) {
				prefs.put("firstAppStartUp", "No");
			}

			return isFirstAppStartUp;
		}

	}

	// returns path from which jar file with this class runs
	// works correctly both when run from executable jar, and from IDE
	private Path GetExecutionPath(Object obj) throws URISyntaxException {

		File jarDir = new File(
				ClassLoader.getSystemClassLoader().getResource(".").toURI().getPath());
		Path absolutePath = jarDir.toPath();
		logger.info("Settings.GetExecutionPath() returned: " + absolutePath.toString());

		return absolutePath;
	}
}

package ru.codeuniverse.audiocrammer.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.SystemUtils;
import org.controlsfx.control.StatusBar;
import org.controlsfx.control.textfield.TextFields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.dao.Dao;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.AudioRecord;
import ru.codeuniverse.audiocrammer.model.AudioRecordDAOManager;
import ru.codeuniverse.audiocrammer.model.Backlog;
import ru.codeuniverse.audiocrammer.model.ReviewTracker;
import ru.codeuniverse.audiocrammer.model.Settings;
import ru.codeuniverse.audiocrammer.model.Statistics;
import ru.codeuniverse.audiocrammer.model.Topics;

public class MainViewController implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(MainViewController.class);

	// XXX
	// is set from Backlog.addFromDir as quick tmp decision
	// REFACTOR THIS LOGIC!
	public static long totalDurationMinOfAddedToBacklog;

	private Dao<AudioRecord, Integer> audioRecordDAO;

	private AudioRecord recordToAdjust;

	private Settings settings;

	private StatusBar statusBar;
	private Label statusBarCountAllFilesLabel;
	private Label statusBarCountNotCheckedOutLabel;
	private Label statusBarCountInProgressLabel;
	private Label statusBarCountDueForReviewLabel;

	@FXML
	private CheckBox saveAsNewDefaultTopicSettingsCheckBox;

	@FXML
	private BorderPane sceneRootPane;

	@FXML
	private TextField adjustTextField;

	@FXML
	private CheckBox editingTopicsDisabledCheckBox;

	// @FXML
	// private TextField topicGroupTextField;
	//
	// @FXML
	// private TextField topicTextField;
	//
	// @FXML
	// private TextField subTopicTextField;

	@FXML
	private Button autoFill_One_Btn;

	@FXML
	private Button autoFill_Two_Btn;

	@FXML
	private Button autoFill_Three_Btn;

	@FXML
	private Button checkoutNewBtn;

	@FXML
	private Button adjustBtn;

	@FXML
	private Button addToBacklogBtn;

	@FXML
	private ComboBox<String> topicGroupComboBox;

	@FXML
	private ComboBox<String> topicComboBox;

	@FXML
	private ComboBox<String> subTopicComboBox;

	public MainViewController() {
		this.settings = new Settings();
	}

	public AudioRecord getAdjustRecord() {
		return recordToAdjust;
	}

	@FXML
	void autoFill_One_BtnHandler(ActionEvent event) {
		AutoFillSupport autoFillSupport = new AutoFillSupport();
		AutoFillEntry autoFillEntry = autoFillSupport.getEntry_One();
		boolean autoFillEntryIsNull = (null == autoFillEntry);

		if (autoFillEntryIsNull) {
			// do nth
			return;
		}

		boolean topicGroupIsNull = (autoFillEntry.getTopicGroup() == null);
		if (topicGroupIsNull) {
			// do nth
			return;
		}

		boolean topicGroupIsEmpty = (autoFillEntry.getTopicGroup().trim() == "");

		if (topicGroupIsEmpty) {
			// do nth
			return;
		} else {
			topicGroupComboBox.setValue(autoFillEntry.getTopicGroup());
			topicComboBox.setValue(autoFillEntry.getTopic());
			subTopicComboBox.setValue(autoFillEntry.getSubTopic());
		}
	}

	@FXML
	void autoFill_Two_BtnHandler(ActionEvent event) {
		AutoFillSupport autoFillSupport = new AutoFillSupport();
		AutoFillEntry autoFillEntry = autoFillSupport.getEntry_Two();
		boolean autoFillEntryIsNull = (null == autoFillEntry);

		if (autoFillEntryIsNull) {
			// do nth
			return;
		}

		boolean topicGroupIsNull = (autoFillEntry.getTopicGroup() == null);
		if (topicGroupIsNull) {
			// do nth
			return;
		}

		boolean topicGroupIsEmpty = (autoFillEntry.getTopicGroup().trim() == "");

		if (topicGroupIsEmpty) {
			// do nth
			return;
		} else {
			topicGroupComboBox.setValue(autoFillEntry.getTopicGroup());
			topicComboBox.setValue(autoFillEntry.getTopic());
			subTopicComboBox.setValue(autoFillEntry.getSubTopic());
		}
	}

	@FXML
	void autoFill_Three_BtnHandler(ActionEvent event) {
		AutoFillSupport autoFillSupport = new AutoFillSupport();
		AutoFillEntry autoFillEntry = autoFillSupport.getEntry_Three();
		boolean autoFillEntryIsNull = (null == autoFillEntry);

		if (autoFillEntryIsNull) {
			// do nth
			return;
		}

		boolean topicGroupIsNull = (autoFillEntry.getTopicGroup() == null);
		if (topicGroupIsNull) {
			// do nth
			return;
		}

		boolean topicGroupIsEmpty = (autoFillEntry.getTopicGroup().trim() == "");

		if (topicGroupIsEmpty) {
			// do nth
			return;
		} else {
			topicGroupComboBox.setValue(autoFillEntry.getTopicGroup());
			topicComboBox.setValue(autoFillEntry.getTopic());
			subTopicComboBox.setValue(autoFillEntry.getSubTopic());
		}
	}

	@FXML
	void addToBackLogBtnAction(ActionEvent event) {
		if (topicGroupComboBox.getValue() == null) {
			showTopicGroupCannotBeEmptyMsg();
			return;
		}
		if ((topicGroupComboBox.getValue().trim()).equals("")) {
			showTopicGroupCannotBeEmptyMsg();
			return;
		}
		Window ownerWindow = ((Button) event.getSource()).getScene().getWindow();
		File dirToProcess = getDirectoryWithFilesToAddToBacklog(ownerWindow);
		try {
			Backlog backlog = new Backlog();
			Topics topics = new Topics();
			topics.setTopicGroup(topicGroupComboBox.getValue().trim());
			topics.setTopic(topicComboBox.getValue().trim());
			topics.setSubTopic(subTopicComboBox.getValue().trim());
			int numberOfItemsAdded = backlog.addFromDir(dirToProcess, topics);
			if (numberOfItemsAdded == 0) {
				showNothingAddedToBacklogMsg();
			} else {
				showNFilesSuccessfullyAddedToBacklogMsg(numberOfItemsAdded, topics);
			}
		} catch (IOException e) {
			// TODO MSg
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Msg
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Msg call getCause() to get SQLException
			e.printStackTrace();
		}

		// change default topics, if allowed
		boolean isEditingAllowed = !editingTopicsDisabledCheckBox.isSelected();
		boolean isSaveSettingsWanted = saveAsNewDefaultTopicSettingsCheckBox.isSelected();

		if (isEditingAllowed && isSaveSettingsWanted) {
			String topicGroupDefault = topicGroupComboBox.getValue();
			settings.setTopicGroupDefault(topicGroupDefault);

			String topicDefault = topicComboBox.getValue();
			settings.setTopicDefault(topicDefault);

			String subTopicDefault = subTopicComboBox.getValue();
			settings.setSubTopicDefault(subTopicDefault);

			AutoFillSupport autoFillSupport = new AutoFillSupport();
			autoFillSupport.addEntry(topicGroupDefault, topicDefault, subTopicDefault);

		}

		// restore default topics after this button pressed
		restoreDefaultTopics();

	}

	private void showTopicGroupCannotBeEmptyMsg() {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("TOPIC GROUP CANNOT BE EMPTY!");
		alert.setHeaderText("Please, specify Topic Group");
		alert.setContentText("You should specify Topic Group! ");
		alert.showAndWait();
	}

	private void showNFilesSuccessfullyAddedToBacklogMsg(int numberOfItemsAdded,
			Topics topics) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("SUCCESSFULLY ADDED TO BACKLOG!");
		alert.setHeaderText(numberOfItemsAdded + "  file(s) successfully added."
				+ System.lineSeparator() + "Toal duration:  " + totalDurationMinOfAddedToBacklog
				+ " min." + System.lineSeparator() + "Topic Group:  " + topics.getTopicGroup()
				+ System.lineSeparator() + "Topic:  " + topics.getTopic()
				+ System.lineSeparator() + "subTopic:  " + topics.getSubTopic());
		alert.setContentText("Now you can Check Out New");
		alert.showAndWait();
	}

	private void showNothingAddedToBacklogMsg() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("NOTHING ADDED TO BACKLOG!");
		alert.setHeaderText("SOURCE DIRECTORY HAS NO VALID FILES, " + System.lineSeparator()
				+ "OR YOU ARE ADDING DUPLICATES");
		alert.setContentText("Either directory has no supported audiofiles, "
				+ "or file(s) with same modified time and size were already added to Backlog earlier.");
		alert.showAndWait();
	}

	private void restoreDefaultTopics() {
		String topicGroupDefault = settings.getTopicGroupDefault();
		topicGroupComboBox.setValue(topicGroupDefault);

		String topicDefault = settings.getTopicDefault();
		topicComboBox.setValue(topicDefault);

		String subTopicDefault = settings.getSubTopicDefault();
		subTopicComboBox.setValue(subTopicDefault);

		editingTopicsDisabledCheckBox.setSelected(true);
		saveAsNewDefaultTopicSettingsCheckBox.setSelected(false);
	}

	private File getDirectoryWithFilesToAddToBacklog(Window ownerWindow) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		String dialogMessage = "Open Directory with files to add to Repetition Backlog";
		directoryChooser.setTitle(dialogMessage);
		// Directory with files to add to Repetition Backlog
		File dirToProcess;
		dirToProcess = directoryChooser.showDialog(ownerWindow);
		return dirToProcess;
	}

	@FXML
	void adjustBtnAction(ActionEvent event) {
		if (isEmptyInputOfAdjustTextField()) {
			showEmptyInputInAdjustFieldMessage();
			return;
		}

		// search DB for AudioRecord with name as in adjustTextField
		recordToAdjust = getRecordToAdjust();

		if (recordToAdjust == null) {
			showNoFileToAdjustFoundMessage();
			return;
		}

		createInitAndShowAdjustStage();

	}

	private void showEmptyInputInAdjustFieldMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("EMPTY INPUT");
		alert.setHeaderText("INPUT IS EMPTY !");
		alert.setContentText("Please input audio file name without extension.");
		alert.showAndWait();
	}

	private void showNoFileToAdjustFoundMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("NO SUCH FILE FOUND");
		alert.setHeaderText("There is no file with such name in DB!");
		alert.setContentText(
				"Close this window (Cancel) and try input another file name (without extention)");
		alert.showAndWait();
	}

	/**
	 * @return AudioRecord with name as in adjustTextField (+ extension). If no match found (or
	 *         2 and more matches found) returns null.
	 */
	private AudioRecord getRecordToAdjust() {
		// Get DAO object - audioRecordDAO
		// No need to close - it is "singleton" connection,
		// closed at app shutdown (see FXMain.stop() method)
		AudioRecordDAOManager audioRecordDAOManager;
		try {
			audioRecordDAOManager = new AudioRecordDAOManager();
			audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Query DB for file with name: adjustTextField.getText() + ".WMA"
		// Save found retrieved objects in List<AudioRecord> audioRecord
		// XXX PROBLEM IF ANOTHER EXTENSION LATER USED !!!
		String trackedName = adjustTextField.getText().trim() + ".WMA";
		List<AudioRecord> audioRecord = new ArrayList<>();
		try {
			audioRecord = audioRecordDAO.queryForEq("trackedName", trackedName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// found single entry, so everything goes normal
		if (audioRecord.size() == 1) {
			return audioRecord.get(0);
		}

		return null; // found nothing or multiple entries
	}

	private void createInitAndShowAdjustStage() {
		// load root (adjustRecordRoot) from "AdjustRecordView.fxml"
		URL url = getClass().getResource("AdjustRecordView.fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(url);
		BorderPane adjustRecordRoot = null;
		try {
			adjustRecordRoot = (BorderPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("AdjustRecordView pane root is " + adjustRecordRoot);
		Scene adjustRecordScene = new Scene(adjustRecordRoot);
		adjustRecordScene.getStylesheets()
				.add(getClass().getResource("application.css").toExternalForm());

		Stage adjustRecordStage = new Stage();
		adjustRecordStage.setScene(adjustRecordScene);
		adjustRecordStage.setMaximized(false);
		String windowTitle = "Adjust Record";
		adjustRecordStage.setTitle(windowTitle);
		setIcon(adjustRecordStage);
		Stage primaryStage = (Stage) sceneRootPane.getScene().getWindow();
		adjustRecordStage.initOwner(primaryStage);
		adjustRecordStage.show();
	}

	private boolean isEmptyInputOfAdjustTextField() {
		if (adjustTextField.getText().trim().equals("")) {
			return true;
		}

		return false;
	}

	@FXML
	void checkoutNewBtnAction(ActionEvent event) throws SQLException {
		ReviewTracker reviewTracker = new ReviewTracker();
		if (reviewTracker.hasScheduledNotReviewed()) {
			// SHOW MSG Checkout New NOT Allowed. Please review all due first!
			// http://code.makery.ch/blog/javafx-dialogs-official/
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("CANNOT CHECKOUT NEW!");
			alert.setHeaderText("Click CHECKOUT FOR REPETITION first!");
			alert.setContentText("Please review all due (scheduled for repetition) first! "
					+ "You still have scheduled for repetition but not reviewed files!");
			alert.showAndWait();
			return;
		}

		int totalDuration = 0;
		Backlog backlog = new Backlog();
		try {
			Settings settings = new Settings();
			int maxDuration = settings.getMaxDuration();
			totalDuration = backlog.checkoutNew(maxDuration);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Alert alert = new Alert(AlertType.INFORMATION);
		if (totalDuration == 0) {
			// SHOW MSG: THERE IS NOTHING IN BACKLOG TO CHECK-OUT
			alert.setTitle("CANNOT CHECKOUT NEW!");
			alert.setHeaderText("Click \"ADD FILES TO BACKLOG\" first!");
			alert.setContentText("There are no files in Backlog. " + System.lineSeparator()
					+ "Please add new files to backlog first!" + System.lineSeparator()
					+ System.lineSeparator() + "FILES IN USER_OUTPUT_DIR MAY BE OBSOLETE "
					+ System.lineSeparator() + "(written there at LAST \"CHECKOUT NEW\")");
			alert.showAndWait();
		} else {
			// SHOW MSG: successfully checked-out n files
			Settings settings = new Settings();
			alert.setTitle("SUCCESSFULLY CHECKED-OUT NEW!");
			alert.setHeaderText("Total Duration (min): " + totalDuration);
			alert.setContentText(
					"See your " + settings.getTodayStudyAudioOutputDir() + " directory.");
			alert.showAndWait();

		}

	}

	@FXML
	void checkoutNewManuallyBtnAction(ActionEvent event) {

	}

	@FXML
	void getForReviewBtnAction(ActionEvent event) throws Exception {
		ReviewTracker reviewTracker = new ReviewTracker();
		Alert alert = new Alert(AlertType.INFORMATION);
		List<AudioRecord> scheduledButNotReviewedRecords = new ArrayList<>();
		scheduledButNotReviewedRecords = reviewTracker.getAllScheduledNotReviewed();
		if (scheduledButNotReviewedRecords.isEmpty()) {
			// SHOW MSG: nothing to review
			alert.setTitle("NOTHING TO REVIEW!");
			alert.setHeaderText("Wait until next due review date or Check-Out New");
			alert.setContentText(
					"There are no files with scheduled review date today or before today.");
			alert.showAndWait();
			return;
		}

		// if there are files to review
		// TODO DO PROCESSING
		int duration;
		duration = reviewTracker.checkoutAllScheduledWithOverdue();
		//
		// SHOW MSG: duration of files you have to review:
		alert.setTitle("DURATION OF FILES YOU HAVE TO REVIEW:");
		alert.setHeaderText(duration + " min");
		alert.setContentText("Please review all these files before checking out new.");
		alert.showAndWait();
	}

	@FXML
	void uncheckBtnAction(ActionEvent event) {

	}

	@FXML
	void onMainMenuGeneralSettings(ActionEvent event) {
		showMainMenuGeneralSettingsWindow();
	}

	private void showMainMenuGeneralSettingsWindow() {
		URL url = getClass().getResource("SettingsView.fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(url);
		AnchorPane generalSettingsRoot = null;
		try {
			generalSettingsRoot = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("Settings pane root is " + generalSettingsRoot);
		Scene generalSettingsScene = new Scene(generalSettingsRoot);
		generalSettingsScene.getStylesheets()
				.add(getClass().getResource("application.css").toExternalForm());

		Stage generalSettingsStage = new Stage();
		generalSettingsStage.setScene(generalSettingsScene);
		generalSettingsStage.setMaximized(false);
		String windowTitle = "Settings";
		generalSettingsStage.setTitle(windowTitle);
		setIcon(generalSettingsStage);
		generalSettingsStage.initOwner(FXMain.getPrimaryStage());
		generalSettingsStage.show();
	}

	@FXML
	void onMainMenuMasterSettings(ActionEvent event) {
		showMainMenuMasterSettingsWindow();
	}

	private void showMainMenuMasterSettingsWindow() {
		URL url = getClass().getResource("MasterSettingsView.fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(url);
		AnchorPane masterSettingsRoot = null;
		try {
			masterSettingsRoot = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("Settings pane root is " + masterSettingsRoot);
		Scene masterSettingsScene = new Scene(masterSettingsRoot);
		masterSettingsScene.getStylesheets()
				.add(getClass().getResource("application.css").toExternalForm());

		Stage masterSettingsStage = new Stage();
		masterSettingsStage.setScene(masterSettingsScene);
		masterSettingsStage.setMaximized(false);
		String windowTitle = "Master Settings";
		masterSettingsStage.setTitle(windowTitle);
		setIcon(masterSettingsStage);
		masterSettingsStage.initOwner(FXMain.getPrimaryStage());
		masterSettingsStage.show();
	}

	// TODO impl logic
	// so far just opens Cram folder...
	@FXML
	void getForCrammingBtnAction(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			Settings settings = new Settings();
			String folderToOpen = settings.getCramThisOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	void openCramThisFolderBtn(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			Settings settings = new Settings();
			String folderToOpen = settings.getCramThisOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	void openTodayStudyAudioFolderBtn(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			Settings settings = new Settings();
			String folderToOpen = settings.getTodayStudyAudioOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@FXML
	void onMainMenuStatistics(ActionEvent event) {
		showMainMenuStatisticsWindow();

	}

	private void showMainMenuStatisticsWindow() {
		URL url = getClass().getResource("StatisticsView.fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(url);
		AnchorPane statisticsRoot = null;
		try {
			statisticsRoot = (AnchorPane) loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("Statistics pane root is " + statisticsRoot);
		Scene statisticsScene = new Scene(statisticsRoot);
		statisticsScene.getStylesheets()
				.add(getClass().getResource("application.css").toExternalForm());

		Stage statisticsStage = new Stage();

		statisticsStage.setScene(statisticsScene);
		statisticsStage.setMaximized(false);
		String windowTitle = "Statistics";
		statisticsStage.setTitle(windowTitle);
		setIcon(statisticsStage);
		statisticsStage.initOwner(FXMain.getPrimaryStage());
		statisticsStage.show();
	}

	private void setIcon(Stage stage) {
		InputStream appIconImg = getClass().getResourceAsStream(Settings.APP_ICON);
		Image appIcon = new Image(appIconImg);
		stage.getIcons().add(appIcon);
	}

	@FXML
	void onMainMenuOnlineHelp(ActionEvent event) {
		FXMain.hostServices.showDocument(Settings.HELP_URL);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initApp();

		addStatusBar();
		initStatusBar();
		updateStatusBar();

		addTerminateAppOnESCHandler();

		addDisableTopicsEditingBindingToComboBoxes();

		initDefaultTopicsComboBoxes();

		initToolTips();

		promptUserAtFirstAppStartUp();

	}

	private void promptUserAtFirstAppStartUp() {
		if (!Settings.FirstAppStart.isFirstAppStart()) {
			// do nothing - this is not first app start
			return;
		}

		// Currently do nothing
		// first start-up requires no initial settings from user

		logger.info(
				"First app start-up detected. Currently do nothing - first start-up requires no initial settings from user");

	}

	private void initToolTips() {
		Tooltip adjustToolTip = new Tooltip();
		String adjustToolTipText = "Enter tracked file name without .WAV extension";
		adjustToolTip.setText(adjustToolTipText);
		adjustTextField.setTooltip(adjustToolTip);
		adjustBtn.setTooltip(adjustToolTip);

		Tooltip checkoutNewToolTip = new Tooltip();
		String checkoutNewToolTipText = "New Files to Study are added to your TODAY STUDY AUDIO Folder!";
		checkoutNewToolTip.setText(checkoutNewToolTipText);
		checkoutNewBtn.setTooltip(checkoutNewToolTip);

		Tooltip addToBacklogToolTip = new Tooltip();
		String addToBacklogToolTipText = "All .WAV files in target folder will be imported";
		addToBacklogToolTip.setText(addToBacklogToolTipText);
		addToBacklogBtn.setTooltip(addToBacklogToolTip);

	}

	private void initDefaultTopicsComboBoxes() {
		String topicGroupDefault = settings.getTopicGroupDefault();
		topicGroupComboBox.setValue(topicGroupDefault);
		topicGroupComboBox.setEditable(true);

		String topicDefault = settings.getTopicDefault();
		topicComboBox.setValue(topicDefault);
		topicComboBox.setEditable(true);

		String subTopicDefault = settings.getSubTopicDefault();
		subTopicComboBox.setValue(subTopicDefault);
		subTopicComboBox.setEditable(true);

		// to init all ComboBoxes before they got any focus
		addExistingTopicGroupsToComboBox();
		addExistingTopicsToComboBox();

		// add on focused ChangeListener to all three topic ComboBoxes
		topicGroupComboBox.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
					Boolean newValue) {
				if (newValue == true) {
					// our topicGroupComboBox gained focus
					addExistingTopicGroupsToComboBox();
				}
			}
		});

		topicComboBox.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
					Boolean newValue) {
				if (newValue == true) {
					// our topicComboBox gained focus
					addExistingTopicsToComboBox();
				}
			}
		});

		subTopicComboBox.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
					Boolean newValue) {
				if (newValue == true) {
					// our subTopicComboBox gained focus
					addExistingSubTopicsToComboBox();
				}
			}
		});

		// enable auto-completion for ComboBoxes
		TextFields.bindAutoCompletion(topicGroupComboBox.getEditor(),
				topicGroupComboBox.getItems());
		TextFields.bindAutoCompletion(topicComboBox.getEditor(), topicComboBox.getItems());
		TextFields.bindAutoCompletion(subTopicComboBox.getEditor(),
				subTopicComboBox.getItems());
	}

	void addExistingTopicGroupsToComboBox() {
		logger.debug("addExistingTopicGroupsToComboBox() entered");
		if (audioRecordDAO == null) {
			// init audioRecordDAO, if it was not init before
			AudioRecordDAOManager audioRecordDAOManager;
			try {
				audioRecordDAOManager = new AudioRecordDAOManager();
				audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
			} catch (SQLException e) {
				logger.info(
						"To init topicGroupComboBox with default values tried initialize audioRecordDAO and got SQLException.");
				e.printStackTrace();
			}

		}

		Set<String> existingTopicGroups = new TreeSet<>();
		for (AudioRecord audioRecord : audioRecordDAO) {
			existingTopicGroups.add(audioRecord.getTopicGroup());
		}

		for (String existingTopicGroup : existingTopicGroups) {
			if (!topicGroupComboBox.getItems().contains(existingTopicGroup)) {
				logger.debug(
						"addExistingTopicGroupsToComboBox() updated topicGroupComboBox with existing Topic Group. Topic Group = "
								+ existingTopicGroup);
				topicGroupComboBox.getItems().add(existingTopicGroup);
			}
		}

		// enable auto-completion for topicGroupComboBox
		TextFields.bindAutoCompletion(topicGroupComboBox.getEditor(),
				topicGroupComboBox.getItems());
		// topicGroupComboBox.requestFocus();
		// TextFields.bindAutoCompletion(topicGroupComboBox.getEditor(),
		// existingTopicGroups.toArray());

	}

	void addExistingTopicsToComboBox() {
		logger.debug("addExistingTopicsToComboBox() entered");
		topicComboBox.getItems().clear();
		if (audioRecordDAO == null) {
			// init audioRecordDAO, if it was not init before
			AudioRecordDAOManager audioRecordDAOManager;
			try {
				audioRecordDAOManager = new AudioRecordDAOManager();
				audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
			} catch (SQLException e) {
				logger.info(
						"To init topicComboBox with default values tried initialize audioRecordDAO and got SQLException.");
				e.printStackTrace();
			}

		} else {
			// now audioRecordDAO is sure initialized
			Set<String> existingTopics = new TreeSet<>();
			String currentTopicGroup = topicGroupComboBox.getValue().trim();
			for (AudioRecord audioRecord : audioRecordDAO) {
				if ((audioRecord.getTopicGroup().trim()).equals(currentTopicGroup)) {
					existingTopics.add(audioRecord.getTopic());
				}
			}

			for (String existingTopic : existingTopics) {
				if (!topicComboBox.getItems().contains(existingTopic)) {
					logger.debug(
							"addExistingTopicToComboBox() updated topicComboBox with existing Topic. Topic = "
									+ existingTopic);
					topicComboBox.getItems().add(existingTopic);
				}
			}
		}

		// enable auto-completion for topic ComboBox
		TextFields.bindAutoCompletion(topicComboBox.getEditor(), topicComboBox.getItems());
	}

	void addExistingSubTopicsToComboBox() {
		logger.debug("addExistingSubTopicsToComboBox() entered");
		subTopicComboBox.getItems().clear();
		if (audioRecordDAO == null) {
			// init audioRecordDAO, if it was not init before
			AudioRecordDAOManager audioRecordDAOManager;
			try {
				audioRecordDAOManager = new AudioRecordDAOManager();
				audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
			} catch (SQLException e) {
				logger.info(
						"To init subTopicComboBox with default values tried initialize audioRecordDAO and got SQLException.");
				e.printStackTrace();
			}

		} else {
			// now audioRecordDAO is sure initialized
			Set<String> existingSubTopics = new TreeSet<>();
			String currentTopic = null;
			if (topicComboBox.getValue() != null) {
				currentTopic = topicComboBox.getValue().trim();
			}
			String currentTopicGroup = null;
			if (topicGroupComboBox.getValue() != null) {
				currentTopicGroup = topicGroupComboBox.getValue().trim();
			}
			for (AudioRecord audioRecord : audioRecordDAO) {
				boolean isTopicGroupMatched = (audioRecord.getTopicGroup().trim())
						.equals(currentTopicGroup);
				boolean isTopicMatched = (audioRecord.getTopic().trim()).equals(currentTopic);
				if (isTopicGroupMatched && isTopicMatched) {
					existingSubTopics.add(audioRecord.getSubTopic());
				}
			}

			for (String existingSubTopic : existingSubTopics) {
				if (!subTopicComboBox.getItems().contains(existingSubTopic)) {
					logger.debug(
							"addExistingSubTopicToComboBox() updated subTopicComboBox with existing SubTopic. SubTopic = "
									+ existingSubTopic);
					subTopicComboBox.getItems().add(existingSubTopic);
				}
			}
		}

		// enable auto-completion for topic ComboBox
		TextFields.bindAutoCompletion(subTopicComboBox.getEditor(),
				subTopicComboBox.getItems());
	}

	private void addDisableTopicsEditingBindingToComboBoxes() {

		topicGroupComboBox.disableProperty()
				.bind(editingTopicsDisabledCheckBox.selectedProperty());

		topicComboBox.disableProperty().bind(editingTopicsDisabledCheckBox.selectedProperty());

		subTopicComboBox.disableProperty()
				.bind(editingTopicsDisabledCheckBox.selectedProperty());

		saveAsNewDefaultTopicSettingsCheckBox.disableProperty()
				.bind(editingTopicsDisabledCheckBox.selectedProperty());

		autoFill_One_Btn.disableProperty()
				.bind(editingTopicsDisabledCheckBox.selectedProperty());

		autoFill_Two_Btn.disableProperty()
				.bind(editingTopicsDisabledCheckBox.selectedProperty());

		autoFill_Three_Btn.disableProperty()
				.bind(editingTopicsDisabledCheckBox.selectedProperty());

		// restore defaults when editingTopicsDisabledCheckBox checked (again)
		editingTopicsDisabledCheckBox.selectedProperty()
				.addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable,
							Boolean oldValue, Boolean newValue) {
						if (newValue == true) {
							initDefaultTopicsComboBoxes();
							saveAsNewDefaultTopicSettingsCheckBox.setSelected(false);
						}
					}
				});
	}

	// on ESC pressed close this view and terminate app
	private void addTerminateAppOnESCHandler() {

		sceneRootPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {

				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage thisStage = (Stage) wnd;
				// to prevent closing app
				// when ESCaping auto-completion prompt in ComboBox
				boolean isTopicGroupComboBoxFocused = topicGroupComboBox.isFocused();
				boolean isTopicComboBoxFocused = topicComboBox.isFocused();
				boolean isSubTopicComboBoxFocused = subTopicComboBox.isFocused();
				boolean shallNotCloseApp = isTopicGroupComboBoxFocused | isTopicComboBoxFocused
						| isSubTopicComboBoxFocused;

				if (isTopicGroupComboBoxFocused) {
					topicGroupComboBox.getEditor().cancelEdit();
				} else if (isTopicComboBoxFocused) {
					topicComboBox.getEditor().cancelEdit();
				} else if (isSubTopicComboBoxFocused) {
					subTopicComboBox.getEditor().cancelEdit();
				}

				if (shallNotCloseApp) {
					// do nothing
				} else {
					thisStage.hide();
				}
			}
		});
	}

	// creates folders for internal use, unless they already exist
	private void initApp() {

		Settings settings = new Settings();

		Path appDir = Paths.get(settings.getAppDir());
		Path trackedAudioDir = Paths.get(settings.getTrackedAudioDir());
		Path todayStudyAudioOutputDir = Paths.get(settings.getTodayStudyAudioOutputDir());
		Path discontinuedDir = Paths.get(settings.getDiscontinuedDir());
		Path crammingDir = Paths.get(settings.getCramThisOutputDir());

		try {
			if (Files.notExists(appDir)) {
				Files.createDirectories(appDir);
			}
			if (Files.notExists(trackedAudioDir)) {
				Files.createDirectories(trackedAudioDir);
			}
			if (Files.notExists(todayStudyAudioOutputDir)) {
				Files.createDirectories(todayStudyAudioOutputDir);
			}
			if (Files.notExists(discontinuedDir)) {
				Files.createDirectories(discontinuedDir);
			}
			if (Files.notExists(crammingDir)) {
				Files.createDirectories(crammingDir);
			}
		} catch (IOException e) {
			// TODO: handle exception
		}

		// create dummy file for AutoFillSupport to avoid FileNotFoundException
		if (!Files.exists(settings.getAutoFillPersistedFile().toPath())) {
			try {
				Files.createFile(settings.getAutoFillPersistedFile().toPath());
				new AutoFillSupport().init();
			} catch (IOException e1) {
				logger.info(
						"initApp() in MainViewController threw IOException while creating dummy file for AutoFillSupport");
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	public void updateStatusBar() {
		// int countAllFiles = 0;
		// int countNotCheckedOut = 0;
		// int countInProgress = 0; // all checked-out
		// try {
		// Backlog backlog = new Backlog();
		// countAllFiles = backlog.countAllFiles();
		// countNotCheckedOut = backlog.countNotCheckedOut();
		// countInProgress = countAllFiles - countNotCheckedOut;
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// int countDueForReview = 0; // already due to be reviewed
		// try {
		// ReviewTracker reviewTracker = new ReviewTracker();
		// countDueForReview = reviewTracker.countScheduledNotReviewed();
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		Statistics stats = null;
		Backlog backlog = null;
		try {
			backlog = new Backlog();
			stats = backlog.getStatistics();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// setting statusBarCountAllFilesLabel
		String msg1 = "Total: ";
		String text1 = msg1 + stats.getAllFilesDuration() + " min.    ";
		statusBarCountAllFilesLabel.setText(text1);

		// setting statusBarCountNotCheckedOutLabel
		String msg2 = "Not Checked-Out: ";
		String text2 = msg2 + stats.getNotCheckedOutDuration() + " min.  ";
		statusBarCountNotCheckedOutLabel.setText(text2);

		// setting statusBarCountInProgressLabel
		String msg3 = "Checked-Out: ";
		String text3 = msg3 + stats.getCheckedOutDuration() + " min.   ";
		statusBarCountInProgressLabel.setText(text3);

		// setting statusBarCountDueForReviewLabel
		String msg4 = "Due for Review (all): ";
		String text4 = msg4 + stats.getScheduledNotReviewedAllDuration() + " min.  ";
		statusBarCountDueForReviewLabel.setText(text4);
	}

	private void initStatusBar() {
		statusBar.setText(null);

		statusBarCountAllFilesLabel = new Label();
		statusBarCountNotCheckedOutLabel = new Label();
		statusBarCountInProgressLabel = new Label();
		statusBarCountDueForReviewLabel = new Label();

		statusBar.getRightItems().add(statusBarCountDueForReviewLabel);
		statusBar.getLeftItems().add(statusBarCountAllFilesLabel);
		statusBar.getLeftItems().add(statusBarCountNotCheckedOutLabel);
		statusBar.getLeftItems().add(statusBarCountInProgressLabel);
	}

	private void addStatusBar() {
		statusBar = new StatusBar();
		sceneRootPane.setBottom(statusBar);
	}

	// just to make sure that connection is closed for AudioRecord DAO
	void closeDaoConnection() {
		try {
			AudioRecordDAOManager audioRecordDAOManager = new AudioRecordDAOManager();
			// returns ref to unique DAO instance for AudioRecord class
			Dao<AudioRecord, Integer> audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
			audioRecordDAO.getConnectionSource().close();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			logger.warn(
					"IOException closing audioRecordDAO (in MainViewController.closeDaoConnection()");
			ioe.printStackTrace();
		} catch (SQLException sqle) {
			// TODO Auto-generated catch block
			logger.warn(
					"SQLException closing audioRecordDAO (in MainViewController.closeDaoConnection()");
			sqle.printStackTrace();
		}
	}

}

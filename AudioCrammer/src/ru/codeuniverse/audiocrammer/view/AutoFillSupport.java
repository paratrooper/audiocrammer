package ru.codeuniverse.audiocrammer.view;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.codeuniverse.audiocrammer.model.Settings;

public class AutoFillSupport implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(AutoFillSupport.class);

	private Settings settings;

	private String autoFillPersistedFileName;

	public void addEntry(String topicGroup, String topic, String subTopic) {

		AutoFill autoFill;
		try {
			autoFill = readAutoFill();
		} catch (FileNotFoundException e) {
			// Create dummy file
			autoFill = new AutoFill();
			writeAutoFill(new AutoFill());

		}
		if (autoFill == null) {
			autoFill = new AutoFill();
		}

		autoFill.addTopicGroup(topicGroup);
		autoFill.addTopic(topic);
		autoFill.addSubTopic(subTopic);

		writeAutoFill(autoFill);
	}

	public AutoFillEntry getEntry_One() {
		AutoFill autoFill;
		try {
			autoFill = readAutoFill();
		} catch (FileNotFoundException e) {
			logger.info("AutoFillEntry getEntry_One() returned null on FileNotFoundException.");
			return null;
		}

		if (autoFill == null) {
			return null;
		}

		AutoFillEntry autoFillEntry = new AutoFillEntry();
		autoFillEntry.setTopicGroup(autoFill.getTopicGroup_1());
		autoFillEntry.setTopic(autoFill.getTopic_1());
		autoFillEntry.setSubTopic(autoFill.getSubTopic_1());

		return autoFillEntry;
	}

	public AutoFillEntry getEntry_Two() {
		AutoFill autoFill;
		try {
			autoFill = readAutoFill();
		} catch (FileNotFoundException e) {
			logger.info("AutoFillEntry getEntry_Two() returned null on FileNotFoundException.");
			return null;
		}
		if (autoFill == null) {
			return null;
		}

		AutoFillEntry autoFillEntry = new AutoFillEntry();
		autoFillEntry.setTopicGroup(autoFill.getTopicGroup_2());
		autoFillEntry.setTopic(autoFill.getTopic_2());
		autoFillEntry.setSubTopic(autoFill.getSubTopic_2());

		return autoFillEntry;
	}

	public AutoFillEntry getEntry_Three() {
		AutoFill autoFill;
		try {
			autoFill = readAutoFill();
		} catch (FileNotFoundException e) {
			logger.info(
					"AutoFillEntry getEntry_Three() returned null on FileNotFoundException.");
			return null;
		}

		if (autoFill == null) {
			return null;
		}

		AutoFillEntry autoFillEntry = new AutoFillEntry();
		autoFillEntry.setTopicGroup(autoFill.getTopicGroup_3());
		autoFillEntry.setTopic(autoFill.getTopic_3());
		autoFillEntry.setSubTopic(autoFill.getSubTopic_3());

		return autoFillEntry;
	}

	private void writeAutoFill(AutoFill autoFill) {
		settings = new Settings();
		File autoFillPersistedFile = settings.getAutoFillPersistedFile();

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(autoFillPersistedFile);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(autoFill);
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info("writeAutoFill.addEntry caused FileNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("writeAutoFill.addEntry caused IOException");
			e.printStackTrace();
		}
	}

	public void init() {
		AutoFill autoFill = new AutoFill();
		writeAutoFill(autoFill);
	}

	// returns null if attempt to read was unsuccessful
	private AutoFill readAutoFill() throws FileNotFoundException {
		settings = new Settings();
		File autoFillPersistedFile = settings.getAutoFillPersistedFile();

		FileInputStream fis;
		AutoFill autoFill = null;
		try {
			fis = new FileInputStream(autoFillPersistedFile);
			BufferedInputStream bis = new BufferedInputStream(fis);
			ObjectInputStream ois = new ObjectInputStream(bis);
			autoFill = (AutoFill) ois.readObject();
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("AutoFillSupport.readAutoFill caused IOException");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info("AutoFillSupport.readAutoFill caused ClassNotFoundException");
			e.printStackTrace();
		}

		return autoFill;
	}

}

package ru.codeuniverse.audiocrammer.view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.Settings;

public class SettingsViewController implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(SettingsViewController.class);

	@FXML
	private AnchorPane rootAnchorPane;

	@FXML
	private Button openOutputDirBtn;

	@FXML
	private Label internalDirLbl;

	// app settings
	private Settings settings;

	@FXML
	private TextField maxDurationTextField;

	@FXML
	private Label todayAudioFolderLabel;

	@FXML
	private Label todayAudioSavedMsgLabel;

	@FXML
	void applyBtnAction(ActionEvent event) {

		String maxDuration = maxDurationTextField.getText();

		boolean isInputValid;
		boolean isInputNumeric;
		boolean isInputGreaterOrEqualMinLimit;
		isInputNumeric = StringUtils.isNumeric(maxDuration);
		if (!isInputNumeric) {
			showMsgMaxDurationIsNotValidNumeric();
			return;
		}

		System.out.println("NUUUUUUUUUUUUUUUUU: isInputNumeric:  " + isInputNumeric);
		if (Integer.valueOf(maxDuration) >= Settings.MAX_DURATION_MIN_LIMIT) {
			isInputGreaterOrEqualMinLimit = true;
		} else {
			isInputGreaterOrEqualMinLimit = false;
		}

		if (isInputNumeric && isInputGreaterOrEqualMinLimit) {
			String durationToSet = maxDurationTextField.getText();
			int maxDurationToSet = Integer.valueOf(durationToSet);
			settings.setMaxDuration(maxDurationToSet);
		} else {
			showMsgMaxDurationIsNotValidNumeric();
			return;
		}

		// close window
		Window wnd = ((Node) event.getSource()).getScene().getWindow();
		Stage settingsStage = (Stage) wnd;
		settingsStage.hide();
		return;
	}

	public SettingsViewController() {
		settings = new Settings();

	}

	@FXML
	void openTodayStudyAudioDirBtnAction(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			String folderToOpen = settings.getTodayStudyAudioOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@FXML
	void openCramThisDirBtnAction(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			String folderToOpen = settings.getCramThisOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	void maxDurationKeyTypedHandler(KeyEvent event) {

	}

	@FXML
	void maxDurationKeyReleasedHandler(KeyEvent event) {

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.debug("Entered SettingsViewController initialize() method");
		String internalFolderName = settings.getAppDir();
		String text = internalDirLbl.getText() + "  " + internalFolderName;
		internalDirLbl.setText(text);

		String maxDuration = String.valueOf(settings.getMaxDuration());
		maxDurationTextField.setText(maxDuration);

		String todayAudioFolder = settings.getTodayStudyAudioOutputDir();
		todayAudioFolderLabel.setText(todayAudioFolder);

		// on ESC close adjustRecord form and do nothing
		rootAnchorPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage settingsStage = (Stage) wnd;
				settingsStage.hide();
			}
		});
	}

	// positive numeric value >= 25 is allowed
	private void showMsgMaxDurationIsNotValidNumeric() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("PLEASE ENTER A VALID NUMBER");
		String headerText = "Please enter a positive number >= "
				+ settings.MAX_DURATION_MIN_LIMIT;
		alert.setHeaderText(headerText);
		alert.setContentText(headerText);
		alert.showAndWait();
	}

	@FXML
	void selectTodayAudioFolderBtnHandler(ActionEvent event) {

		Node source = (Node) event.getSource();
		Stage thisStage = (Stage) source.getScene().getWindow();

		DirectoryChooser directoryChooser = new DirectoryChooser();
		File selectedDirectory = directoryChooser.showDialog(thisStage);

		if (selectedDirectory == null) {
			// do nothing
		} else {
			String todayAudioFolder = selectedDirectory.getAbsolutePath();
			todayAudioFolderLabel.setText(todayAudioFolder);
			settings.setTodayStudyAudioOutputDir(todayAudioFolder);
			todayAudioSavedMsgLabel.setVisible(true);
		}
	}

}

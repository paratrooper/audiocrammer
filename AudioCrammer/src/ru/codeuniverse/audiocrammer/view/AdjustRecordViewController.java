package ru.codeuniverse.audiocrammer.view;

import java.io.IOException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.math3.util.Precision;
import org.apache.commons.validator.routines.DateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.dao.Dao;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.AudioRecord;
import ru.codeuniverse.audiocrammer.model.AudioRecordDAOManager;
import ru.codeuniverse.audiocrammer.model.Importance;
import ru.codeuniverse.audiocrammer.model.ReviewTracker;
import ru.codeuniverse.audiocrammer.model.Settings;

public class AdjustRecordViewController implements Initializable {

	private static final Logger logger = LoggerFactory
			.getLogger(AdjustRecordViewController.class);

	AudioRecord audioRecordToAdjust;

	// Stage for this Controller
	private Stage adjustStage;

	@FXML
	private BorderPane rootBorderPane;

	@FXML
	private RadioButton highRadioBtn;

	@FXML
	private ToggleGroup ImportanceToggleGroup;

	@FXML
	private RadioButton lowRadioBtn;

	@FXML
	private RadioButton midRadioBtn;

	@FXML
	private CheckBox cramCheckBox;

	@FXML
	private CheckBox discontinueCheckBox;

	@FXML
	private TextField nextReviewDateTextField;

	@FXML
	private TextField nextReviewDateInDaysTextField;

	@FXML
	private TextField adjustedNextReviewInDaysTextField;

	@FXML
	private TextField searchTagsTextField;

	@FXML
	private TextField subTopicTextField;

	@FXML
	private TextArea CommentTextArea;

	@FXML
	private TextField nextReviewNumberTextField;

	@FXML
	private TextField idTextField;

	@FXML
	private TextField trackedNameTextField;

	@FXML
	private TextField origModifiedTimeTextField;

	@FXML
	private TextField fileSizeTextField;

	@FXML
	private TextField durationTextField;

	@FXML
	private TextArea actualReviewHistoryTextField;

	@FXML
	private TextField topicTextField;

	@FXML
	private TextField topicGroupTextField;

	@FXML
	private TextField adjustedNextReviewISODateTextField;

	@FXML
	private Label discontinueLabel;

	@FXML
	private Label searchTagsLabel;

	@FXML
	private Label fileSizeLabel;

	@FXML
	private Label nextReviewDateInDaysLabel;

	@FXML
	private Label nextReviewDateInDaysAdjustedLabel;

	@FXML
	void applyBtnAction(ActionEvent event) {
		audioRecordToAdjust.setComment(CommentTextArea.getText().trim());
		audioRecordToAdjust.setSearchTags(searchTagsTextField.getText().trim());
		audioRecordToAdjust.setSubTopic(subTopicTextField.getText().trim());

		if (discontinueCheckBox.isSelected()) {
			ReviewTracker reviewTracker;
			try {
				reviewTracker = new ReviewTracker();
				reviewTracker.discontinue(audioRecordToAdjust);
				FXMain.mainViewController.updateStatusBar();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info(
						"applyBtnAction(): SQL Exception  trying to discontinue audioRecord");
				e.printStackTrace();
			} catch (Exception e) {
				logger.info("applyBtnAction(): Exception trying to discontinue audioRecord");
				e.printStackTrace();
			}

			Window wnd = ((Node) event.getSource()).getScene().getWindow();
			Stage adjustRecordStage = (Stage) wnd;
			adjustRecordStage.hide();
			return;

		}

		if (cramCheckBox.isSelected()) {
			audioRecordToAdjust.setCramFlag(true);
			try {
				copyFileFromTrackedToCrammingDir(audioRecordToAdjust);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.info("Cannot copy file to cram from TrackedAudioDir to CrammingDir");
				e.printStackTrace();
			}
		} else {
			audioRecordToAdjust.setCramFlag(false);
			// remove file from Cram Folder if it is there
			Settings settings = new Settings();
			String fileName = audioRecordToAdjust.getTrackedName();
			String crammingDir = settings.getCramThisOutputDir();
			Path filePathInCrammingDir = Paths.get(crammingDir, fileName);
			try {
				Files.deleteIfExists(filePathInCrammingDir);

			} catch (IOException e) {
				logger.info(
						"Cannot delete file from crammingDir (cram chrckbox was unchecked)");
			}

		}

		if (midRadioBtn.isSelected()) {
			audioRecordToAdjust.setImportance(Importance.MID);
		} else if (highRadioBtn.isSelected()) {
			audioRecordToAdjust.setImportance(Importance.HIGH);
		} else if (lowRadioBtn.isSelected()) {
			audioRecordToAdjust.setImportance(Importance.LOW);
		}

		// sets audioRecordToAdjust.setNextReviewDate
		adjustNextReviewDate();

		// write all changes to DB
		AudioRecordDAOManager audioRecordDAOManager;
		Dao<AudioRecord, Integer> audioRecordDAO;
		try {
			audioRecordDAOManager = new AudioRecordDAOManager();
			audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
			audioRecordDAO.update(audioRecordToAdjust);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Window wnd = ((Node) event.getSource()).getScene().getWindow();
		Stage adjustRecordStage = (Stage) wnd;
		adjustRecordStage.hide();
	}

	// returns simple file name (w/o path) of copied file
	private String copyFileFromTrackedToCrammingDir(AudioRecord audioRecord)
			throws IOException {
		String fileName = audioRecord.getTrackedName();
		Settings settings = new Settings();
		String trackedAudioDir = settings.getTrackedAudioDir();
		Path trackedPathName = Paths.get(trackedAudioDir, fileName);
		String crammingDir = settings.getCramThisOutputDir();
		Path crammingPathName = Paths.get(crammingDir, fileName);
		try {
			Files.copy(trackedPathName, crammingPathName, StandardCopyOption.COPY_ATTRIBUTES);

		} catch (FileAlreadyExistsException e) {
			logger.info("Cannot copy file from trackedAudioDir to crammingDir");
		}

		return fileName;
	}

	@FXML
	void cancelBtnAction(ActionEvent event) {

		Window wnd = ((Node) event.getSource()).getScene().getWindow();
		Stage adjustRecordStage = (Stage) wnd;
		adjustRecordStage.hide();
	}

	@FXML
	void OnAdjustedNextReviewInDaysTextFieldChange(KeyEvent event) {
		if (!isNextReviewInDaysInputValid()) {
			showInvalidNextReviewInDaysInputMessage();
			adjustedNextReviewInDaysTextField.clear();

		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		initToolTips();

		audioRecordToAdjust = FXMain.mainViewController.getAdjustRecord();

		trackedNameTextField.setText(audioRecordToAdjust.getTrackedName());
		idTextField.setText(String.valueOf(audioRecordToAdjust.getId()));

		Importance importance = audioRecordToAdjust.getImportance();
		if (importance == Importance.MID) {
			midRadioBtn.setSelected(true);
		} else if (importance == Importance.HIGH) {
			highRadioBtn.setSelected(true);
		} else if (importance == Importance.LOW) {
			lowRadioBtn.setSelected(true);
		}

		boolean isCramEnabled = audioRecordToAdjust.isCramFlag();
		if (isCramEnabled) {
			cramCheckBox.setSelected(true);
		} else {
			cramCheckBox.setSelected(false);
		}

		String nextReviewDate = audioRecordToAdjust.getNextReviewDate();
		nextReviewDateTextField.setText(nextReviewDate);

		String nextReviewNumber = String.valueOf(audioRecordToAdjust.getNextReviewNumber());
		nextReviewNumberTextField.setText(nextReviewNumber);

		String searchTags = audioRecordToAdjust.getSearchTags();
		searchTagsTextField.setText(searchTags);

		String subTopic = audioRecordToAdjust.getSubTopic();
		subTopicTextField.setText(subTopic);

		String topic = audioRecordToAdjust.getTopic();
		topicTextField.setText(topic);

		String topicGroup = audioRecordToAdjust.getTopicGroup();
		topicGroupTextField.setText(topicGroup);

		String comment = audioRecordToAdjust.getComment();
		CommentTextArea.setText(comment);

		String actualReviewHistory = audioRecordToAdjust.getActualReviewHistory();
		actualReviewHistoryTextField.setText(actualReviewHistory);

		String fileSizeInMegabytes;
		double bytesInMegabyte = 1024.0 * 1024.0;
		double fileSizeInMegabytesDouble = audioRecordToAdjust.getFileSize() / bytesInMegabyte;
		fileSizeInMegabytes = String.valueOf(Precision.round(fileSizeInMegabytesDouble, 1));
		fileSizeTextField.setText(fileSizeInMegabytes);

		String origModifiedTime = audioRecordToAdjust.getOrigModifiedTime();
		origModifiedTimeTextField.setText(origModifiedTime);

		String durationInMinutes = String
				.valueOf((int) audioRecordToAdjust.getFileSize() / Settings.MINUTE_HAS_BYTES);
		durationTextField.setText(durationInMinutes);

		int nDays = calculateDaysUntilNextReview();
		nextReviewDateInDaysTextField.setText(String.valueOf(nDays));

		// on ESC close adjustRecord form and do nothing
		rootBorderPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage adjustRecordStage = (Stage) wnd;
				adjustRecordStage.hide();
			}
		});

	}

	private void initToolTips() {
		Tooltip discontinueToolTip = new Tooltip();
		String discontinueToolTipText = "Deletes record from DB and Tracked Audio Dir, "
				+ "and moves it to Discontinued Folder. " + "DOES NOT DELETE FROM CRAM FOLDER, "
				+ "SO UNCHECK CRAM FIRST, IF NEED TO DELETE FROM CRAM FOLDER!";
		discontinueToolTip.setText(discontinueToolTipText);
		discontinueCheckBox.setTooltip(discontinueToolTip);
		discontinueLabel.setTooltip(discontinueToolTip);

		Tooltip cramToolTip = new Tooltip();
		String cramToolTipText = "Checking this box - adds (unchecking - deletes) file from Cramming Folder. Works only if file is tracked (not discontinued). Does not affect tracked folder.";
		cramToolTip.setText(cramToolTipText);
		cramCheckBox.setTooltip(cramToolTip);

		Tooltip searchTagsToolTip = new Tooltip();
		String searchTagsToolTipText = "Comma separated. Leading and trailing whitespaces (before and after each comma) ignored";
		searchTagsToolTip.setText(searchTagsToolTipText);
		searchTagsTextField.setTooltip(searchTagsToolTip);
		searchTagsLabel.setTooltip(searchTagsToolTip);

		Tooltip fileSizeToolTip = new Tooltip();
		String fileSizeToolTipText = "In real MegaBytes (MibiBytes)";
		fileSizeToolTip.setText(fileSizeToolTipText);
		fileSizeTextField.setTooltip(fileSizeToolTip);
		fileSizeLabel.setTooltip(fileSizeToolTip);

		Tooltip nextReviewNumberToolTip = new Tooltip();
		String nextReviewNumberToolTipText = "Check-out itself is zero, so after checkout next review number is 1 (first time you review after check-out study)";
		nextReviewNumberToolTip.setText(nextReviewNumberToolTipText);
		nextReviewNumberTextField.setTooltip(nextReviewNumberToolTip);

		Tooltip nextReviewDateInDaysToolTip = new Tooltip();
		String nextReviewDateInDaysToolTipText = "Counts full days in-between: not including last checkout\review day and next review day";
		nextReviewDateInDaysToolTip.setText(nextReviewDateInDaysToolTipText);
		nextReviewDateInDaysTextField.setTooltip(nextReviewDateInDaysToolTip);
		nextReviewDateInDaysLabel.setTooltip(nextReviewDateInDaysToolTip);

		Tooltip adjustedNextReviewToolTip = new Tooltip();
		String adjustedNextReviewToolTipText = "number of full days in-between from today (not counting today and reviewDay";
		adjustedNextReviewToolTip.setText(adjustedNextReviewToolTipText);
		adjustedNextReviewInDaysTextField.setTooltip(adjustedNextReviewToolTip);
		nextReviewDateInDaysAdjustedLabel.setTooltip(adjustedNextReviewToolTip);
	}

	private void adjustNextReviewDate() {

		boolean isDaysTextFieldNotEmpty = !adjustedNextReviewInDaysTextField.getText().trim()
				.isEmpty();
		boolean isISODateTextFieldNotEmpty = !adjustedNextReviewISODateTextField.getText()
				.trim().isEmpty(); // adjustedNextReviewISODateTextField

		if (isDaysTextFieldNotEmpty && isISODateTextFieldNotEmpty) {
			showAmbiguousNextReviewInputMessage();
			return;
		}

		if (isISODateTextFieldNotEmpty) {
			if (!isISOReviewDateInputValid()) {
				showISOReviewDateInputInvalidMessage();
				return;
			}
			audioRecordToAdjust
					.setNextReviewDate(adjustedNextReviewISODateTextField.getText().trim());
			return;
		}

		// otherwise convert nDays from today to yyyy-MM-double ISO 8601
		String nextReviewISODate = audioRecordToAdjust.getNextReviewDate();
		Date nextReviewDateAsDate = null;
		try {
			nextReviewDateAsDate = convertISOToDate(nextReviewISODate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String dateStr = adjustedNextReviewInDaysTextField.getText();
		int plusDays = 0;
		if (!dateStr.isEmpty() && dateStr.matches("\\d+")) {
			plusDays = Integer.valueOf(adjustedNextReviewInDaysTextField.getText());
		} else {
			return;
		}

		String ISONextReviewAdjustedDate;
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		// +1, I need [today, plusDays, TargetDate]
		Date adjustedReviewDateAsDate = NDaysPlusDate(today, plusDays + 1);
		ISONextReviewAdjustedDate = DateFormatUtils.ISO_DATE_FORMAT
				.format(adjustedReviewDateAsDate);

		audioRecordToAdjust.setNextReviewDate(ISONextReviewAdjustedDate);
	}

	private void showISOReviewDateInputInvalidMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("INVALID NEXT REVIEW ISO DATE INPUT!");
		alert.setHeaderText(
				"Next review date was not changed, all other changes were applied!");
		alert.setContentText("Only digits allowed and pattern shall be like this: 2017-02-28");
		alert.showAndWait();
	}

	private boolean isISOReviewDateInputValid() {
		String input = adjustedNextReviewISODateTextField.getText().trim();

		if (input.isEmpty()) {
			return true;
		}

		// year cannot be less than 2017
		if (input.substring(0, 4).matches("[0-9]{4}")) {
			if (Integer.valueOf(input.substring(0, 4)) < 2017) {
				return false;
			}
		} else {
			return false;
		}

		DateValidator dateValidator = new DateValidator();
		Date parsedDate = dateValidator.validate(input, "yyyy-MM-dd");
		boolean matchesISOPattern = (parsedDate == null) ? false : true;

		// we intentionally don't check if date goes before today
		// if user sets such early date, it means he requests immediate review
		// so later card would be scheduled for review as soon as possible
		return matchesISOPattern;
	}

	private void showInvalidNextReviewInDaysInputMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("INVALID NEXT REVIEW IN DAYS INPUT!");
		alert.setHeaderText(
				"Invalid input: interval in days must contain only digits, shall be integer and cannot be zero or negative.");
		alert.setContentText("Please try again to input correct value!");
		alert.showAndWait();
	}

	private boolean isNextReviewInDaysInputValid() {
		String input = adjustedNextReviewInDaysTextField.getText().trim();
		if (input.isEmpty()) {
			return true;
		}
		boolean isNumeric = StringUtils.isNumeric(input); // 2.3 returns false
		if (!isNumeric) {
			return false;
		}
		int numericInput = Integer.valueOf(input);
		boolean isPositive = numericInput > 0;
		boolean isInputValid = isNumeric && isPositive;

		return isInputValid;
	}

	private void showAmbiguousNextReviewInputMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AMBIGUOUS NEXT REVIEW INPUT!");
		alert.setHeaderText(
				"Next review date WAS NOT CHANGED! All OTHER CHANGES were APPLIED!");
		alert.setContentText(
				"Input either days from now interval, or ISO date of next review. Cannot set both days from now interval, and ISO date of next review. Set only one of them!");
		alert.showAndWait();
	}

	/**
	 * @return number of days, not including last checkout\review day and next review day
	 *         (counts full days strictly in-between)
	 */
	private int calculateDaysUntilNextReview() {
		String nextReviewDate;
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();

		nextReviewDate = audioRecordToAdjust.getNextReviewDate();
		Date nextReview = null;
		try {
			nextReview = convertISOToDate(nextReviewDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if nextReview not scheduled, return 0
		if (nextReview == null) {
			return 0;
		}
		int daysDifference = (int) getDaysBetweenDates(today, nextReview);

		return daysDifference;
	}

	/**
	 * @param earlier
	 *            - earlier Date (say, Jan 12)
	 * @param later
	 *            - later Date (say, Jan 26)
	 * @return how many days (24 hours) are between 2 Dates
	 */
	private long getDaysBetweenDates(Date earlier, Date later) {
		long diff = later.getTime() - earlier.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	/**
	 * @param String
	 *            with ISO 8601 date in pattern yyyy-MM-dd
	 * @return Date
	 * @throws ParseException
	 */
	public Date convertISOToDate(String ISO_yyyy_MM_dd_String) throws ParseException {
		if (ISO_yyyy_MM_dd_String == null) {
			return null;
		}

		Date date = DateUtils.parseDate(ISO_yyyy_MM_dd_String, new String[] { "yyyy-MM-dd" });
		return date;
	}

	private Date NDaysPlusDate(Date date, int nDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, nDays);

		return new Date(cal.getTimeInMillis());
	}
}
